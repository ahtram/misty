module gitlab.com/ahtram/misty

go 1.16

require (
	cloud.google.com/go v0.30.0
	github.com/bwmarrin/discordgo v0.18.0
	github.com/fatih/color v1.7.0
	github.com/golang/protobuf v1.2.0
	github.com/gorilla/websocket v1.4.0
	github.com/julienschmidt/httprouter v1.2.0
	github.com/mattn/go-colorable v0.0.9
	github.com/mattn/go-isatty v0.0.4
	golang.org/x/crypto v0.0.0-20181015023909-0c41d7ab0a0e
	golang.org/x/net v0.0.0-20181017193950-04a2e542c03f
	golang.org/x/oauth2 v0.0.0-20181017192945-9dcd33a902f4
	golang.org/x/sys v0.0.0-20181021155630-eda9bb28ed51
	google.golang.org/api v0.0.0-20181021000519-a2651947f503
	google.golang.org/appengine v1.2.0
)
