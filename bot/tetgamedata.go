package bot

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/ahtram/misty/gshelp"
)

// TETGameData contains all data we read from the game data Google Sheets.
type TETGameData struct {
	lStringData   TETGameLStringData
	devRecipeData TETGameDevRecipeData
	itemData      TETGameItemData
}

func (tetGameData *TETGameData) sync(conf botConfig) {
	tetGameData.lStringData.sync(conf)
	tetGameData.itemData.sync(conf)
	tetGameData.devRecipeData.sync(conf)
}

//====

// TETGameLStringData is LString data we read from the game data Google Sheet.
type TETGameLStringData struct {
	contentMap map[string][]string
}

// sync stuffs from Google Sheet.
func (tetGameLStringData *TETGameLStringData) sync(conf botConfig) {
	fmt.Print("Syncing LString Data...")

	workSheetXMLContent, err := fetchFeed(conf.TETGameLStringSheetURL())

	// All tabs' GSheetData.
	sheetData := []gshelp.GSheetData{}

	if err != nil {
		//Oh carp!
		fmt.Println(Red("[Error] ") + err.Error())
	} else {
		fmt.Println(Green("[Complete]"))
		URLs := gshelp.WorkSheetFeedToCellFeedURLs(workSheetXMLContent)

		// Get all cellfeeds.
		for i, URL := range URLs {
			fmt.Print("[Fetching Tab] : [" + strconv.Itoa(i) + "]...")
			cellXMLContent, err := fetchFeed(URL)
			if err != nil {
				fmt.Println(Red("[Error] ") + err.Error())
			} else {
				tabData := gshelp.CellFeedToGSheetData(cellXMLContent)

				// Store in the golbal var.
				sheetData = append(sheetData, tabData)
				fmt.Println(Green("[Complete]"))
			}
		}
	}

	// This will empty this container.
	tetGameLStringData.contentMap = make(map[string][]string)

	// Iterate through tabs.
	for _, sheetTab := range sheetData {
		// Iterate through rows.
		for columnIndex, row := range sheetTab.StringTable {
			// Check if each row has an ID.
			if columnIndex > 0 && len(row) > 0 {
				if row[0] != "" {
					// Add this row.
					tetGameLStringData.contentMap[row[0]] = row[1:5]
				}
			}
		}
	}
}

// LocalizedString returns the localized string of strID.
func (tetGameLStringData *TETGameLStringData) LocalizedString(strID string, lang Lang) string {
	if value, exist := tetGameLStringData.contentMap[strID]; exist && int(lang) < len(value) {
		return value[lang]
	}
	return ""
}

//====

// TETGameDevRecipe represents a DevRecipe.
type TETGameDevRecipe struct {
	ID                     string
	weight                 int
	mixer                  string
	productItemID          string
	mixingMatA             string
	mixingMatB             string
	mixingMatC             string
	mixingMatD             string
	rewardRepBase          int
	randomFeaturePatternID string
}

// LocalizedProductName returns the localized name string of the recipe product item.
func (tetGameDevRecipe *TETGameDevRecipe) LocalizedProductName(lang Lang, tetGameItemData TETGameItemData, tetGameLStringData TETGameLStringData) string {
	return tetGameItemData.LocalizedName(tetGameDevRecipe.productItemID, lang, tetGameLStringData)
}

// LocalizedInfo returns the localized name string of the recipe.
func (tetGameDevRecipe *TETGameDevRecipe) LocalizedInfo(lang Lang, tetGameItemData TETGameItemData, tetGameLStringData TETGameLStringData) string {
	// Form localized info string for this Recipe.
	returnStr := "["
	returnStr += tetGameLStringData.LocalizedString("Mixer_"+tetGameDevRecipe.mixer, lang)
	returnStr += "]: "

	// Product
	returnStr += "**" + tetGameItemData.LocalizedName(tetGameDevRecipe.productItemID, lang, tetGameLStringData) + "**"
	returnStr += " => \n"

	// MatA
	if len(tetGameDevRecipe.mixingMatA) > 0 {
		returnStr += tetGameItemData.LocalizedName(tetGameDevRecipe.mixingMatA, lang, tetGameLStringData)
	}

	// MatB
	if len(tetGameDevRecipe.mixingMatB) > 0 {
		returnStr += " + "
		returnStr += tetGameItemData.LocalizedName(tetGameDevRecipe.mixingMatB, lang, tetGameLStringData)
	}

	// MatC
	if len(tetGameDevRecipe.mixingMatC) > 0 {
		returnStr += " + "
		returnStr += tetGameItemData.LocalizedName(tetGameDevRecipe.mixingMatC, lang, tetGameLStringData)
	}

	// MatD
	if len(tetGameDevRecipe.mixingMatD) > 0 {
		returnStr += " + "
		returnStr += tetGameItemData.LocalizedName(tetGameDevRecipe.mixingMatD, lang, tetGameLStringData)
	}

	// fmt.Println("LocalizedInfo: " + returnStr)

	return returnStr
}

//====

// TETGameDevRecipeData is DevRecipe data we read from the game data Google Sheet.
type TETGameDevRecipeData struct {
	contentSlice []TETGameDevRecipe
	contentMap   map[string]TETGameDevRecipe
	productIDMap map[string][]TETGameDevRecipe
}

// sync stuffs from Google Sheet.
func (tetGameDevRecipeData *TETGameDevRecipeData) sync(conf botConfig) {
	fmt.Print("Syncing DevRecipe Data...")

	workSheetXMLContent, err := fetchFeed(conf.TETGameDevRecipeSheetURL())

	// All tabs' GSheetData.
	sheetData := []gshelp.GSheetData{}

	if err != nil {
		//Oh carp!
		fmt.Println(Red("[Error] ") + err.Error())
	} else {
		fmt.Println(Green("[Complete]"))
		URLs := gshelp.WorkSheetFeedToCellFeedURLs(workSheetXMLContent)

		// Get all cellfeeds.
		for i, URL := range URLs {
			fmt.Print("[Fetching Tab] : [" + strconv.Itoa(i) + "]...")
			cellXMLContent, err := fetchFeed(URL)
			if err != nil {
				fmt.Println(Red("[Error] ") + err.Error())
			} else {
				tabData := gshelp.CellFeedToGSheetData(cellXMLContent)

				// Store in the golbal var.
				sheetData = append(sheetData, tabData)
				fmt.Println(Green("[Complete]"))
			}
		}
	}

	// This will empty this container. (Slice)
	tetGameDevRecipeData.contentSlice = make([]TETGameDevRecipe, 0)

	// Iterate through tabs.
	for _, sheetTab := range sheetData {
		// Iterate through rows.
		for columnIndex, row := range sheetTab.StringTable {
			// Check if each row has an ID.
			if columnIndex > 1 && len(row) > 0 {
				if row[0] != "" {
					// Add this row.
					devRecipe := TETGameDevRecipe{
						ID:                     row[0],
						mixer:                  row[2],
						productItemID:          row[3],
						mixingMatA:             row[4],
						mixingMatB:             row[6],
						mixingMatC:             row[8],
						mixingMatD:             row[10],
						randomFeaturePatternID: row[14],
					}

					// weight
					devRecipe.weight, _ = strconv.Atoi(row[1])

					// reward rep
					devRecipe.rewardRepBase, _ = strconv.Atoi(row[13])

					// Slice
					tetGameDevRecipeData.contentSlice = append(tetGameDevRecipeData.contentSlice, devRecipe)
				}
			}
		}
	}

	// This will empty this container. (general content map)
	tetGameDevRecipeData.contentMap = make(map[string]TETGameDevRecipe)

	// Iterate through tabs.
	for _, sheetTab := range sheetData {
		// Iterate through rows.
		for _, row := range sheetTab.StringTable {
			// Check if each row has an ID.
			if len(row) > 0 {
				if row[0] != "" {
					// Add this row.
					devRecipe := TETGameDevRecipe{
						ID:                     row[0],
						mixer:                  row[2],
						productItemID:          row[3],
						mixingMatA:             row[4],
						mixingMatB:             row[6],
						mixingMatC:             row[8],
						mixingMatD:             row[10],
						randomFeaturePatternID: row[14],
					}

					// weight
					devRecipe.weight, _ = strconv.Atoi(row[1])

					// reward rep
					devRecipe.rewardRepBase, _ = strconv.Atoi(row[13])

					tetGameDevRecipeData.contentMap[row[0]] = devRecipe
				}
			}
		}
	}

	// This will empty this container. (productItemID index map)
	tetGameDevRecipeData.productIDMap = make(map[string][]TETGameDevRecipe)

	// Iterate through tabs.
	for _, sheetTab := range sheetData {
		// Iterate through rows.
		for _, row := range sheetTab.StringTable {
			// Check if each row has an ID.
			if len(row) > 0 {
				if row[0] != "" {
					// Add this row.
					devRecipe := TETGameDevRecipe{
						ID:                     row[0],
						mixer:                  row[2],
						productItemID:          row[3],
						mixingMatA:             row[4],
						mixingMatB:             row[6],
						mixingMatC:             row[8],
						mixingMatD:             row[10],
						randomFeaturePatternID: row[14],
					}

					// weight
					devRecipe.weight, _ = strconv.Atoi(row[1])

					// reward rep
					devRecipe.rewardRepBase, _ = strconv.Atoi(row[13])

					if len(devRecipe.productItemID) > 0 {
						//Append to map slice.
						tetGameDevRecipeData.productIDMap[devRecipe.productItemID] = append(tetGameDevRecipeData.productIDMap[devRecipe.productItemID], devRecipe)
					}
				}
			}
		}
	}
}

// query from the exist data and return the matched DevRecipe.
func (tetGameDevRecipeData *TETGameDevRecipeData) queryByName(queryProductItemName string, lang Lang, tetGameItemData TETGameItemData, tetGameLStringData TETGameLStringData) []TETGameDevRecipe {
	var returnRecipes = []TETGameDevRecipe{}
	// Search through the slice.
	for i := 0; i < len(tetGameDevRecipeData.contentSlice); i++ {
		productItemName := tetGameDevRecipeData.contentSlice[i].LocalizedProductName(lang, tetGameItemData, tetGameLStringData)

		// fmt.Println("Comparing: " + productItemName)

		if strings.Contains(productItemName, queryProductItemName) {
			// fmt.Println("Bingo! Found the product recipe: " + tetGameDevRecipeData.contentSlice[i].ID)
			// This is the recipe we want.
			returnRecipes = append(returnRecipes, tetGameDevRecipeData.contentSlice[i])
		}
	}

	// fmt.Println("Return returnRecipes length: " + strconv.Itoa(len(returnRecipes)))

	return returnRecipes
}

//====

// TETGameItem represent an item info.
type TETGameItem struct {
	ID               string
	prototypeItemID  string
	iconBase         string
	iconCover        string
	nameID           string
	descID           string
	baseSellPrice    int
	baseQualityScore int
}

//====

// TETGameItemData is Item data we read from the game data Google Sheet.
type TETGameItemData struct {
	contentMap map[string]TETGameItem
}

// sync stuffs from Google Sheet.
func (tetGameItemData *TETGameItemData) sync(conf botConfig) {
	fmt.Print("Syncing Item Data...")

	workSheetXMLContent, err := fetchFeed(conf.TETGameItemSheetURL())

	// All tabs' GSheetData.
	sheetData := []gshelp.GSheetData{}

	if err != nil {
		//Oh carp!
		fmt.Println(Red("[Error] ") + err.Error())
	} else {
		fmt.Println(Green("[Complete]"))
		URLs := gshelp.WorkSheetFeedToCellFeedURLs(workSheetXMLContent)

		// Get all cellfeeds.
		for i, URL := range URLs {
			fmt.Print("[Fetching Tab] : [" + strconv.Itoa(i) + "]...")
			cellXMLContent, err := fetchFeed(URL)
			if err != nil {
				fmt.Println(Red("[Error] ") + err.Error())
			} else {
				tabData := gshelp.CellFeedToGSheetData(cellXMLContent)

				// Store in the golbal var.
				sheetData = append(sheetData, tabData)
				fmt.Println(Green("[Complete]"))
			}
		}
	}

	// This will empty this container. (general content map)
	tetGameItemData.contentMap = make(map[string]TETGameItem)

	// Iterate through tabs.
	for _, sheetTab := range sheetData {
		// Iterate through rows.
		for columnIndex, row := range sheetTab.StringTable {
			// Check if each row has an ID.
			if columnIndex > 1 && len(row) > 0 {
				if row[2] != "" {
					// Add this row.
					item := TETGameItem{
						ID:              row[2],
						prototypeItemID: row[3],
						iconBase:        row[4],
						iconCover:       row[6],
						nameID:          row[8],
						descID:          row[9],
					}

					// baseSellPrice
					item.baseSellPrice, _ = strconv.Atoi(row[13])

					// baseQualityScore
					item.baseQualityScore, _ = strconv.Atoi(row[14])

					tetGameItemData.contentMap[row[2]] = item
				}
			}
		}
	}

}

// LocalizedName returns the localized name string of the item.
func (tetGameItemData *TETGameItemData) LocalizedName(itemID string, lang Lang, tetGameLStringData TETGameLStringData) string {
	if value, exist := tetGameItemData.contentMap[itemID]; exist {
		return tetGameLStringData.LocalizedString(value.nameID, lang)
	}
	fmt.Println("Item ID not exist: " + itemID)
	return ""
}
