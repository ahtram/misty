package bot

import (
	"context"
	"fmt"

	"golang.org/x/oauth2"
	youtube "google.golang.org/api/youtube/v3"
)

const youtubeChannelURLPrefix = "https://www.youtube.com/channel/"

var refreshToken = "1//04k0Zpu6ksCWfCgYIARAAGAQSNwF-L9Irch3FBuCNIqJvRIyXubNcvA_KlLiTEXQfM3BbQnfqthi_3_9pX1bYtk_Gi9_575o-WcA"

var googleOauthConfig = &oauth2.Config {
	ClientID:     "848771126877-le2tv9i62f5147vlnhfn8gr4s3hiaghg.apps.googleusercontent.com",
	ClientSecret: "FXF4UVBOQ-3z04wlDQxnKSML",
	RedirectURL:  "http://gocrazy.uni-team.xyz/",

	Endpoint: oauth2.Endpoint{
		AuthURL:  "https://accounts.google.com/o/oauth2/auth",
		TokenURL: "https://accounts.google.com/o/oauth2/token",
	},

	Scopes: []string{
		"https://www.googleapis.com/auth/userinfo.profile",
		"https://www.googleapis.com/auth/userinfo.email",
		"https://www.googleapis.com/auth/plus.me",
		"https://www.googleapis.com/auth/youtube.readonly",
		"https://www.googleapis.com/auth/youtube",
	},
}

//isYoutubeChannelOnline returns a Beam channel's online status.
func isYoutubeChannelOnline(channelID string) (isOnline bool, err error) {

	ctx := context.Background()

	token := &oauth2.Token{}
	token.RefreshToken = refreshToken

	client := googleOauthConfig.Client(ctx, token)
	service, err := youtube.New(client)
	if err != nil {
		fmt.Printf("isYoutubeChannelOnline Unable to parse client secret to config: %v", err)
		return false, err
	}

	call := service.Search.List("snippet")
	call.ChannelId(channelID)
	call.EventType("live")
	call.Type("video")
	response, err := call.Do()
	if err != nil {
		fmt.Printf("isYoutubeChannelOnline Error making API call: %v", err.Error())
		return false, err
	}

	if len(response.Items) > 0 {
		// fmt.Printf("Youtube response.Items[0].Snippet.ChannelTitle: [%s] | Youtube response.Items count: [%d]\n", response.Items[0].Snippet.ChannelTitle, len(response.Items))
		return true, nil
	}

	// fmt.Println("No live streaming found.")

	// [Dep] nope, this is not the right way
	// call := service.LiveBroadcasts.List("id,snippet,contentDetails,status")
	// call.Mine(true)
	// response, err := call.Do()
	// if err != nil {
	// 	fmt.Printf("isYoutubeChannelOnline Error making API call: %v", err.Error())
	// 	return false, err
	// }

	// if len(response.Items) > 0 {
	// 	fmt.Printf("Youtube response.Items[0].Status.LifeCycleStatus: [%s] | Youtube response.Items count: [%d]\n", response.Items[0].Status.LifeCycleStatus, len(response.Items))
	// 	if response.Items[0].Status.LifeCycleStatus == "live" {
	// 		return true, nil
	// 	}
	// }

	return false, nil
}
