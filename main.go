package main

import (
	"fmt"

	"./bot"
)

func main() {
	// A welcome message with version number.
	bot.PrintWelcomeMessage()

	// Start the bot.
	err := StartBot()
	if err != nil {
		fmt.Println(err.Error())
	}

	return
}

// StartBot gets the bot running.
func StartBot() error {
	//The prime data object.
	misty := bot.Misty{}
	err := misty.Start()
	if err != nil {
		return err
	}

	return nil
}
